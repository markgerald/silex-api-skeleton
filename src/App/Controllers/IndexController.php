<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class IndexController
 * @package App\Controllers
 */
class IndexController
{
    /**
     * @return JsonResponse
     */
    public function indexAction()
    {
        return new JsonResponse(['nome' => 'Mark','email' => 'mark.g.martins@gmail.com']);
    }
    
    public function testeAction($teste)
    {
        return new JsonResponse(['nome' => $teste]);
    }

}