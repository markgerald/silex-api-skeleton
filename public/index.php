<?php
define( 'PATH_ROOT', dirname( __DIR__ ) );
define( 'PATH_VENDOR', PATH_ROOT . '/vendor' );

require_once PATH_VENDOR . '/autoload.php';

$app = new Silex\Application();
include_once PATH_ROOT . '/config/doctrine.php';
include_once PATH_ROOT . '/src/App/register.php';
include_once PATH_ROOT . '/src/App/controllers.php';
include_once PATH_ROOT . '/src/App/routes.php';


$em = $app['orm.em'];


$app['debug'] = true;
$app->run();