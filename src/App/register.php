<?php
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app->register(new \Knp\Provider\ConsoleServiceProvider(), array(
    'console.name'              => 'MyApplication',
    'console.version'           => '1.0.0',
    'console.project_directory' => __DIR__.'/..'
));