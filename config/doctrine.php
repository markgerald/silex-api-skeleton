<?php

use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

$app->register(new DoctrineServiceProvider, array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host'  => 'localhost',
        'dbname' => 'silex',
        'user' => 'root',
        'password' => '',
        'charset' => 'utf8'
    ),
));

$app->register(new DoctrineOrmServiceProvider, array(
    'orm.proxies_dir' =>  __DIR__.'../var/cache/doctrine/proxies',
    'orm.em.options' => array(
        'mappings' => array(
            // Using actual filesystem paths
            array(
                'type' => 'annotation',
                'namespace' => 'App\Entities',
                'path' => __DIR__.'../src/App/Entities',
            ),
        ),
    ),
));

$loader = require __DIR__ . '/../vendor/autoload.php';

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(array($loader, 'loadClass'));