<?php
/**
 * @return \App\Controllers\IndexController
 */
$app['index.controller'] = function() {
    return new \App\Controllers\IndexController();
};